use derive_new::new;
use oauth2::basic::BasicClient;

#[derive(new)]
pub struct AppState {
    pub oauth: BasicClient,
    pub api_base_url: String,
}
