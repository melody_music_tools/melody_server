use actix_web::{get, web, HttpRequest, HttpResponse, Responder};
use actix_web::http::header::{AUTHORIZATION, USER_AGENT};
use actix_web::web::Data;
use autometrics::autometrics;
use jsonwebtoken::{encode, EncodingKey, Header};
use http::HeaderMap;
use mongodb::bson::{doc};
use oauth2::http;
use oauth2::devicecode::StandardDeviceAuthorizationResponse;
use oauth2::http::{HeaderValue};
use oauth2::reqwest::async_http_client;
use url::Url;

use super::{AuthRequest, Claims, LoginInfo};
use crate::{AppState, JWT_SECRET};

#[get("/login")]
#[autometrics]
pub async fn login(request: HttpRequest) -> impl Responder {
    let details: StandardDeviceAuthorizationResponse = request.app_data::<Data<AppState>>()
        .unwrap()
        .oauth
        .exchange_device_code()
        .unwrap()
        .request_async(async_http_client)
        .await
        .unwrap();

    HttpResponse::Found().body(serde_json::to_string(&details).unwrap())
}

#[get("/auth")]
#[autometrics]
pub async fn auth(
    params: web::Query<AuthRequest>,
) -> impl Responder {
    let headers = HeaderMap::from_iter(vec![
        (AUTHORIZATION, HeaderValue::from_str(&format!("token {}", &params.code)).unwrap()),
        (USER_AGENT, HeaderValue::from_str("me").unwrap())
    ]);

    let resp = async_http_client(oauth2::HttpRequest {
        url: Url::parse("https://api.github.com/user").unwrap(),
        method: http::method::Method::GET,
        headers,
        body: Vec::new(),
    }).await;

    let login_info: LoginInfo = serde_json::from_slice(resp.unwrap().body.as_slice()).unwrap();

    let claims = Claims::new(1000000000000000000, login_info.login);
    let token = encode(&Header::default(), &claims, &EncodingKey::from_secret(JWT_SECRET.as_ref()));

    match token {
        Ok(token) => HttpResponse::Ok().body(token),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string())
    }
}
