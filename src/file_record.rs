use chrono::naive::NaiveDateTime;
use derive_new::new;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, new)]
pub struct FileRecord {
    pub username: String,
    pub name: String,
    pub stored_as: String,
    pub upload_time: NaiveDateTime,
}
