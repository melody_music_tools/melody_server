use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Claims {
    upload: String,
}

impl Claims {
    pub fn new(upload: String) -> Self {
        Self {
            upload
        }
    }
}