use std::path::PathBuf;

use derive_new::new;
use serde::Deserialize;

use super::Logging;

#[derive(Deserialize)]
pub enum DatabaseType {
    MongoDB
}

#[derive(Deserialize, new)]
pub struct DatabaseConfiguration {
    pub database_type: DatabaseType,
    pub url: String,
}

impl Default for DatabaseConfiguration {
    fn default() -> Self {
        Self::new(
            DatabaseType::MongoDB,
            "mongodb://localhost:27017".to_string())
    }
}

#[derive(Deserialize, new)]
pub struct GlobalConfiguration {
    #[serde(default = "default_storage_path")]
    pub storage_path: PathBuf,
    #[serde(default = "default_port")]
    pub network_port: u16,
    #[serde(default)]
    pub logging: Logging,
    pub database: DatabaseConfiguration,
    pub oauth_client_id: String,
    pub oauth_client_secret: String,
}

fn default_storage_path() -> PathBuf {
    PathBuf::from("./storage")
}

fn default_port() -> u16 {
    33333
}

impl Default for GlobalConfiguration {
    fn default() -> Self {
        Self::new(
            default_storage_path(),
            default_port(),
            Default::default(),
            Default::default(),
            Default::default(),
            Default::default(),
        )
    }
}

