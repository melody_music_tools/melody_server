use actix_web::dev::ServiceRequest;
use actix_web_httpauth::extractors::AuthenticationError;
use actix_web_httpauth::extractors::bearer::BearerAuth;
use autometrics::autometrics;
use jsonwebtoken::{decode, DecodingKey, Validation};
use crate::JWT_SECRET;

use super::Claims;

#[autometrics]
pub async fn token_validator(request: ServiceRequest, credentials: BearerAuth) -> Result<ServiceRequest, actix_web::Error> {
    let config = request.app_data::<actix_web_httpauth::extractors::bearer::Config>()
        .cloned()
        .unwrap_or_default();

    let decode_result = decode::<Claims>(
        credentials.token(),
        &DecodingKey::from_secret(JWT_SECRET.as_ref()),
        &Validation::default());

    match decode_result {
        Ok(_claims) => Ok(request),
        Err(_error) => Err(AuthenticationError::from(config).into())
    }
}
